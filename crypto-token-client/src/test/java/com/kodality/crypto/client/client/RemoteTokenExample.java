package com.kodality.crypto.client.client;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import org.apache.commons.io.IOUtils;
import org.digidoc4j.Configuration;
import org.digidoc4j.Configuration.Mode;
import org.digidoc4j.Container;
import org.digidoc4j.ContainerBuilder;
import org.digidoc4j.Signature;
import org.digidoc4j.SignatureBuilder;

public class RemoteTokenExample {

  public static void main(String[] args) throws Exception {
    System.setProperty("jdk.internal.httpclient.debug", "true");
    System.setProperty("jdk.internal.httpclient.websocket.debug", "true");
    System.setProperty("jdk.internal.httpclient.hpack.debug", "true");

    Configuration configuration = new Configuration(Mode.TEST);

    Container container = ContainerBuilder
        .aContainer()
        .withConfiguration(configuration)
        .withDataFile(IOUtils.toInputStream("test", StandardCharsets.UTF_8), "test.txt", "text/plain")
        .build();


    char[] passphrase = "Client12345".toCharArray();
    KeyStore ks = KeyStore.getInstance("JKS");
    ks.load(new FileInputStream("C:/Users/Windo/IdeaProjects/crypto/crypto-token-server/src/main/resources/client.jks"), passphrase); // i is an InputStream reading the keystore

    KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
    kmf.init(ks, passphrase);

    TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
    tmf.init(ks);
    RemoteToken remoteToken = new RemoteToken("https://localhost:8443", "id-card", kmf.getKeyManagers(), tmf.getTrustManagers());
    System.out.println("Requesting certificate...");
    X509Certificate certificate = remoteToken.getCertificate();


    Signature signature = SignatureBuilder.
        aSignature(container).
        withSignatureToken(remoteToken).
        invokeSigning();

    container.addSignature(signature);

    container.save(new FileOutputStream("test.bdoc"));
    System.out.println("certificate: " + certificate);
  }
}

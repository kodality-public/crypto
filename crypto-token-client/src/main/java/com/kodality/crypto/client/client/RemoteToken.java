package com.kodality.crypto.client.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kodality.crypto.api.api.CertificateRequest;
import com.kodality.crypto.api.api.CertificateResponse;
import com.kodality.crypto.api.api.SignRequest;
import com.kodality.crypto.api.api.SignResponse;
import com.kodality.crypto.api.api.TokenEndpoints;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.UUID;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import lombok.extern.slf4j.Slf4j;
import org.digidoc4j.DigestAlgorithm;
import org.digidoc4j.SignatureToken;

@Slf4j
public class RemoteToken implements SignatureToken {
  private final URI host;
  private final HttpClient client;
  private final ObjectMapper objectMapper = new ObjectMapper();
  private final String keyAlias;
  private final KeyManager[] keyManagers;
  private final TrustManager[] trustManagers;

  public RemoteToken(String host, String keyAlias, KeyManager[] keyManagers, TrustManager[] trustManagers) {
    this.host = URI.create(host);
    this.keyAlias = keyAlias;
    this.keyManagers = keyManagers;
    this.trustManagers = trustManagers;
    try {
      client = HttpClient.newBuilder()
          .connectTimeout(Duration.ofSeconds(10))
          .version(HttpClient.Version.HTTP_1_1)
          .sslContext(createKeyManager())
          .build();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private SSLContext createKeyManager() {
    try {
      SSLContext sslContext = SSLContext.getInstance("TLS");
      sslContext.init(keyManagers, trustManagers, null);
      return sslContext;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public X509Certificate getCertificate() {
    try {
      CertificateRequest request = new CertificateRequest();
      request.setAlias(keyAlias);
      request.setMessageId(UUID.randomUUID().toString());
      String requestJson = objectMapper.writeValueAsString(request);

      HttpRequest httpRequest = HttpRequest.newBuilder()
          .uri(host.resolve(TokenEndpoints.GET_CERTIFICATE))
          .method("GET", BodyPublishers.ofString(requestJson, StandardCharsets.UTF_8))
          .build();
      HttpResponse<String> httpResponse = client.sendAsync(httpRequest, BodyHandlers.ofString(StandardCharsets.UTF_8)).join();
      if (httpResponse.statusCode() != 200) {
        log.error("Failed to get certificate, reason: {}", httpResponse.body());
        throw new RuntimeException("Unexpected status code :" + httpResponse.statusCode());
      }

      CertificateResponse response = objectMapper.readValue(httpResponse.body(), CertificateResponse.class);
      return response.getCertificate();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public byte[] sign(DigestAlgorithm digestAlgorithm, byte[] dataToSign) {
    try {
      SignRequest request = new SignRequest();
      request.setAlias(keyAlias);
      request.setMessageId(UUID.randomUUID().toString());
      request.setAlgorithm(digestAlgorithm);
      request.setDataToSign(dataToSign);
      String requestJson = objectMapper.writeValueAsString(request);

      HttpRequest httpRequest = HttpRequest.newBuilder()
          .uri(host.resolve(TokenEndpoints.SIGN))
          .method("GET", BodyPublishers.ofString(requestJson, StandardCharsets.UTF_8))
          .build();
      HttpResponse<String> httpResponse = client.sendAsync(httpRequest, BodyHandlers.ofString(StandardCharsets.UTF_8)).join();
      if (httpResponse.statusCode() != 200) {
        log.error("Failed to sign data, reason: {}", httpResponse.body());
        throw new RuntimeException("Unexpected status code :" + httpResponse.statusCode());
      }
      SignResponse response = objectMapper.readValue(httpResponse.body(), SignResponse.class);
      return response.getSignature();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void close() {
    // do nothing
  }
}

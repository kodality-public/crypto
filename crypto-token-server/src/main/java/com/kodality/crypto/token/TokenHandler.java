package com.kodality.crypto.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kodality.crypto.api.api.CertificateRequest;
import com.kodality.crypto.api.api.CertificateResponse;
import com.kodality.crypto.api.api.SignRequest;
import com.kodality.crypto.api.api.SignResponse;
import com.kodality.crypto.api.api.TokenEndpoints;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.digidoc4j.SignatureToken;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

@Slf4j
public class TokenHandler extends AbstractHandler {

  private final SignatureTokenInitializer tokenInitializer;
  private SignatureToken signatureToken;
  private final ObjectMapper objectMapper = new ObjectMapper();


  public TokenHandler(String tokenDriver, int slot, String password) {
    this.tokenInitializer = new SignatureTokenInitializer(tokenDriver, slot, password);
    initToken();
  }

  @Override
  public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException {
    try {
      switch (target) {
        case TokenEndpoints.GET_CERTIFICATE:
          handleGetCertificate(request, response);
          break;
        case TokenEndpoints.SIGN:
          handleSign(request, response);
          break;
        default:
          throw new RuntimeException("Unknown endpoint " + target);
      }
    } catch (Exception e) {
      log.error("Failed to handle request with target {}", target, e);
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
      e.printStackTrace(response.getWriter());
    }

    response.flushBuffer();
    baseRequest.setHandled(true);
  }

  private void handleSign(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
    SignRequest request = objectMapper.readValue(httpRequest.getReader(), SignRequest.class);

    SignResponse response = new SignResponse();
    response.setAlias(request.getAlias());
    response.setMessageId(request.getMessageId());

    byte[] signed;
    try {
      signed = signRequest(request);
    } catch (Exception e){
      log.warn("Failed to sign data, will try to recreate signature token and try again");
      initToken();
      signed = signRequest(request);
    }

    response.setSignature(signed);

    httpResponse.setStatus(HttpServletResponse.SC_OK);
    objectMapper.writeValue(httpResponse.getWriter(), response);
  }

  private void initToken() {
    this.signatureToken = this.tokenInitializer.initToken();
  }

  private byte[] signRequest(SignRequest request) {
    return signatureToken.sign(request.getAlgorithm(), request.getDataToSign());
  }

  private void handleGetCertificate(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
    CertificateRequest request = objectMapper.readValue(httpRequest.getReader(), CertificateRequest.class);

    CertificateResponse response = new CertificateResponse();
    response.setAlias(request.getAlias());
    response.setMessageId(request.getMessageId());
    response.setCertificate(signatureToken.getCertificate());

    httpResponse.setStatus(HttpServletResponse.SC_OK);
    objectMapper.writeValue(httpResponse.getWriter(), response);
  }
}

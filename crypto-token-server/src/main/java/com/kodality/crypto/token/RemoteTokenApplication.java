package com.kodality.crypto.token;


import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.util.ssl.SslContextFactory;

@Slf4j
public class RemoteTokenApplication {

  private static Server initServer(int port, String keyStorePath, String keyStorePassword) {
    System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.Slf4jLog");

    SslContextFactory.Server contextFactory = new SslContextFactory.Server();
    contextFactory.setNeedClientAuth(true);
    contextFactory.setWantClientAuth(true);
    contextFactory.setTrustAll(false);
    contextFactory.setTrustStorePath(keyStorePath);
    contextFactory.setTrustStorePassword(keyStorePassword);
    contextFactory.setKeyStorePath(keyStorePath);
    contextFactory.setKeyStorePassword(keyStorePassword);

    Server server = new Server();
    HttpConfiguration httpsConfig = new HttpConfiguration();
    httpsConfig.addCustomizer(new SecureRequestCustomizer());

    ServerConnector sslConnector = new ServerConnector(server,
        new SslConnectionFactory(contextFactory, HttpVersion.HTTP_1_1.asString()),
        new HttpConnectionFactory(httpsConfig));
    sslConnector.setPort(port);
    server.addConnector(sslConnector);

    return server;
  }

  public static void main(String[] args) throws Exception {
    String tokenDriver = args[0];
    int slot = Integer.parseInt(args[1]);
    String password = args[2].trim();
    int port = Integer.parseInt(args[3]);
    String keyStorePath = args[4];
    String keyStorePassword = args[5];

    Server server = initServer(port, keyStorePath, keyStorePassword);
    server.setHandler(new TokenHandler(tokenDriver, slot, password));
    server.start();
    server.join();
  }
}


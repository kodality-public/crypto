package com.kodality.crypto.api.api;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.kodality.crypto.api.api.util.CryptoUtils;
import java.security.cert.X509Certificate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CertificateResponse extends TokenMessage {
  private X509Certificate certificate;

  @JsonGetter("certificate")
  private String getCertificateBase64() {
    return CryptoUtils.certToBase64(certificate);
  }

  @JsonSetter("certificate")
  private void setCertificateBase64(String base64Cert) {
    certificate = CryptoUtils.base64ToCert(base64Cert);
  }
}

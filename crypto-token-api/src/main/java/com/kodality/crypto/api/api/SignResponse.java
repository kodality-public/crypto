package com.kodality.crypto.api.api;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.Base64;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignResponse extends TokenMessage {
  private byte[] signature;

  @JsonGetter("signature")
  public String getSignatureBae64() {
    return Base64.getEncoder().encodeToString(signature);
  }

  @JsonSetter("signature")
  public void setSignatureBase64(String signatureBase64) {
    signature = Base64.getDecoder().decode(signatureBase64);
  }
}

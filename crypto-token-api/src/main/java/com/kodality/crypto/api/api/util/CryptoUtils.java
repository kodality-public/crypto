package com.kodality.crypto.api.api.util;

import java.io.ByteArrayInputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class CryptoUtils {

  public static String certToBase64(X509Certificate certificate) {
    try {
      byte[] certBytes = certificate.getEncoded();
      return Base64.getEncoder().encodeToString(certBytes);
    } catch (CertificateEncodingException e) {
      throw new RuntimeException("failed to encode certificate to base 64", e);
    }
  }

  public static X509Certificate base64ToCert(String base64Cert) {
    try {
      CertificateFactory factory = CertificateFactory.getInstance("X.509");
      byte[] certBytes = Base64.getDecoder().decode(base64Cert);
      Certificate certificate = factory.generateCertificate(new ByteArrayInputStream(certBytes));
      return (X509Certificate) certificate;
    } catch (CertificateException e) {
      throw new RuntimeException("Error reading certificate: " + e.getMessage(), e);
    }
  }

}

package com.kodality.crypto.api.api;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.Base64;
import lombok.Getter;
import lombok.Setter;
import org.digidoc4j.DigestAlgorithm;

@Getter
@Setter
public class SignRequest extends TokenMessage {
  private DigestAlgorithm algorithm;
  private byte[] dataToSign;

  @JsonGetter("algorithm")
  public String getAlgorithmString() {
    return algorithm != null ? algorithm.name() : null;
  }

  @JsonSetter("algorithm")
  public void setAlgorithmString(String algorithmString) {
    algorithm = algorithmString != null ? DigestAlgorithm.findByAlgorithm(algorithmString) : null;
  }

  @JsonGetter("dataToSign")
  public String getDataToSignBase64() {
    return Base64.getEncoder().encodeToString(dataToSign);
  }

  @JsonSetter("dataToSign")
  public void setDataToSignBase64(String dataToSignBase64) {
    dataToSign = Base64.getDecoder().decode(dataToSignBase64);
  }
}


